function inSo() {
  var hienSoNguyenTo = "";
  var bienSoN = document.getElementById(`numbern`).value * 1;

  if (bienSoN == "") {
    document.getElementById(
      `result-ex5`
    ).innerHTML = `Vui lòng kiểm tra lại thông tin!`;
  } else if (bienSoN <= 1) {
    document.getElementById(
      `result-ex5`
    ).innerHTML = `Không có số nguyên tố nhỏ hơn ${bienSoN}`;
  }

  for (var i = 2; i <= bienSoN; i++) {
    var dieuKien = 0;

    for (var j = 2; j < i; j++) {
      if (i % j == 0) {
        dieuKien = 1;
        break;
      }
    }

    if (i > 1 && dieuKien == 0) {
      hienSoNguyenTo += i + " ";
      document.getElementById(`result-ex5`).innerHTML = hienSoNguyenTo;
    }
  }

  $("#form-5").submit(function (e) {
    e.preventDefault(); // <==stop page refresh==>
  });
}
